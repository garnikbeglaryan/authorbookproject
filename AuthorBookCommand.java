package authorBookProjectq;

public interface AuthorBookCommand {

    String TEXT_BLUE = "\u001B[34m";
    String EXIT = "0";
    String ADD_AUTHOR = "1";
    String SEARCH_AUTHOR = "2";
    String SEARCH_AUTHOR_BY_AGE = "3";
    String PRINT_AUTHORS = "4";
    String ADD_BOOK = "5";
    String SEARCH_BOOK_BY_TITLE = "6";
    String PRINT_BOOK = "7";
    String SEARCH_BOOKS_BY_AUTHOR = "8";
    String COUNTS_BOOK_BY_AUTHOR = "9";
    String CHANGE_AUTHOR = "10";
    String CHANGE_BOOK_AUTHOR = "11";
    String DELETE_AUTHOR = "12";
    String DELETE_BOOK_BY_AUTHOR = "13";


    static void authorBookCommand() {
        System.out.println(TEXT_BLUE + "please input " + EXIT + " for exit ");
        System.out.println("please input " + ADD_AUTHOR + " for add author");
        System.out.println("please input " + SEARCH_AUTHOR + " for search author");
        System.out.println("please input " + SEARCH_AUTHOR_BY_AGE + " for search author by age ");
        System.out.println("Please input " + PRINT_AUTHORS + " for print authors");
        System.out.println("please input " + ADD_BOOK + " for add book");
        System.out.println("please input " + SEARCH_BOOK_BY_TITLE + " for search book");
        System.out.println("please input " + PRINT_BOOK + " for print book");
        System.out.println("please input " + SEARCH_BOOKS_BY_AUTHOR + " for search book by author");
        System.out.println("please input " + COUNTS_BOOK_BY_AUTHOR + " for counts book by author");
        System.out.println("please input " + CHANGE_AUTHOR + " for change author");
        System.out.println("please input " + CHANGE_BOOK_AUTHOR + " for change book author");
        System.out.println("please input " + DELETE_AUTHOR + " for delete author");
        System.out.println("please input " + DELETE_BOOK_BY_AUTHOR + " for delete book by  author");
    }
}
