package authorBookProjectq;


import authorBookProjectq.model.Author;
import authorBookProjectq.model.Book;
import authorBookProjectq.model.Gender;
import authorBookProjectq.storage.AuthorStorage;
import authorBookProjectq.storage.BookStorage;

import java.util.Scanner;

public class AuthorBookTest implements AuthorBookCommand {


    private static Scanner scanner = new Scanner(System.in);
    private static AuthorStorage authorStorage = new AuthorStorage();
    private static BookStorage bookStorage = new BookStorage();

    public static final String TEXT_RED = "\u001B[31m";


    public static void main(String[] args) {


        boolean isRun = true;

        while (isRun) {
            AuthorBookCommand.authorBookCommand();
            String command = scanner.nextLine();
            switch (command) {
                case EXIT:
                    isRun = false;
                    break;
                case ADD_AUTHOR:
                    addAuthor();
                    break;
                case SEARCH_AUTHOR:
                    searchByName();
                    break;
                case SEARCH_AUTHOR_BY_AGE:
                    searchAuthorBYAge();
                    break;
                case PRINT_AUTHORS:
                    authorStorage.print();
                    break;
                case ADD_BOOK:
                    addBook();
                    break;
                case SEARCH_BOOK_BY_TITLE:
                    searchBookByTitle();
                    break;
                case PRINT_BOOK:
                    bookStorage.print();
                    break;
                case SEARCH_BOOKS_BY_AUTHOR:
                    searchBookByAuthor();
                    break;
                case COUNTS_BOOK_BY_AUTHOR:
                    countsBookBYAuthor();
                    break;
                case CHANGE_AUTHOR:
                    changeAuthor();
                    break;
                case CHANGE_BOOK_AUTHOR:
                    changeBookAuthor();
                    break;
                case DELETE_AUTHOR:
                    deleteAuthor();
                    break;
                case DELETE_BOOK_BY_AUTHOR:
                    deleteBookByAuthor();
                    break;
                default:
                    System.out.println(TEXT_RED + "Invalid command");
                    break;
            }
        }
    }

    private static void deleteBookByAuthor() {
        printAuthorsList("please choose book by serialId");
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            bookStorage.deleteByAuthor(author);
        } else {
            System.err.println(TEXT_RED + "Author does not exists");
        }
    }

    private static void deleteAuthor() {
        printAuthorsList("please choose author email");
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            authorStorage.deleteAuthor(author);
        } else {
            System.err.println(TEXT_RED + "Author does not exists");
        }
    }

    private static void printAuthorsList(String please_choose_author_email) {
        System.out.println(please_choose_author_email);
        System.out.println("----------");
        authorStorage.print();
        System.out.println("-----------");
    }

    private static void changeBookAuthor() {
        System.out.println("please choose book by serialId");
        System.out.println("----------");
        bookStorage.print();
        System.out.println("-----------");
        String serialId = scanner.nextLine();
        Book book = bookStorage.getBySerialId(serialId);
        if (book != null) {
            printAuthorsList("please choose author email");
            String email = scanner.nextLine();
            Author author = authorStorage.getByEmail(email);
            if (author != null) {
                book.setAuthor(author);
            } else {
                System.err.println(TEXT_RED + "Author does not exists");
            }
        } else {
            System.err.println(TEXT_RED + "Book with serialId does not exists");
        }
    }

    private static void changeAuthor() {
        printAuthorsList("please choose author email");
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            System.out.println("Please input author name");
            String name = scanner.nextLine();
            System.out.println("please input author surname");
            String surname = scanner.nextLine();
            System.out.println("please input age");
            int age = Integer.parseInt(scanner.nextLine());
            author.setName(name);
            author.setSurname(surname);
            author.setAge(age);
        } else {
            System.out.println(TEXT_RED + "Author does not exists");
        }
    }

    private static void countsBookBYAuthor() {
        printAuthorsList("please choose author email");
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            bookStorage.countByAuthor(author);
        } else {
            System.err.println(TEXT_RED + "Author does not exists");
        }
    }

    private static void searchBookByAuthor() {
        printAuthorsList("please choose author email");
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            bookStorage.searchByAuthor(author);
        } else {
            System.err.println(TEXT_RED + "Author does not exists");
        }
    }

    private static void searchBookByTitle() {
        System.out.println("Please input keyword (title)");
        String keyword = scanner.nextLine();
        bookStorage.searchByTitle(keyword);
    }


    private static void addBook() {
        extracted();
        String email = scanner.nextLine();
        Author author = authorStorage.getByEmail(email);
        if (author != null) {
            System.out.println("please input book's serialId");
            String serialId = scanner.nextLine();
            if (bookStorage.getBySerialId(serialId) == null) {
                System.out.println("Please input book title");
                String title = scanner.nextLine();
                System.out.println("please inout description");
                String description = scanner.nextLine();
                System.out.println("please input price");
                double price = scanner.nextInt();

                Book book = new Book(serialId, title, description, price, author);
                bookStorage.add(book);
                System.out.println("Thank you book was added");
            } else {
                System.err.println("Book with serialId " + serialId + " is exists");
            }
        } else {
            System.out.println(TEXT_RED + "Invalid email! , please try again");
            addBook();
        }
    }

    private static void extracted() {
        printAuthorsList("please choose author email");
    }

    private static void searchAuthorBYAge() {
        System.out.println("Please input minimum age");
        int minAge = Integer.parseInt(scanner.nextLine());
        System.out.println("Please input maximum age");
        int maxAge = Integer.parseInt(scanner.nextLine());
        authorStorage.searchByAge(minAge, maxAge);
    }


    private static void searchByName() {
        System.out.println("Please input keyword");
        String keyword = scanner.nextLine();
        authorStorage.searchByName(keyword);
    }

    private static void addAuthor() {
        System.out.println("Please input author's name,surname,email,age,gender");
        String authorDataStr = scanner.nextLine();
        String[] authorData = authorDataStr.split(",");
        if (authorData.length == 5) {
            int age = Integer.parseInt(authorData[3]);
            Author author = new Author(authorData[0], authorData[1], authorData[2], age, Gender.valueOf(authorData[4]));
            if (authorStorage.getByEmail(author.getEmail()) == null) {
                authorStorage.add(author);
                System.out.println("Thank you author was added");
            } else {
                System.out.println(TEXT_RED + "Invalid email! try again");
            }
        } else {
            System.out.println(TEXT_RED + "Invalid data");
        }
    }
}
