package authorBookProjectq.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Author {

    private String name;
    private String surname;
    private String email;
    private int age;
    private Gender gender;

}
