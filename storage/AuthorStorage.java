package authorBookProjectq.storage;

import authorBookProjectq.AuthorBookTest;
import authorBookProjectq.model.Author;


public class AuthorStorage {

    private Author[] authors = new Author[10];

    int size;


    public void add(Author author) {
        if (authors.length == size) {
            extend();
        }
        authors[size++] = author;

    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(authors[i]);
        }
    }

    private void extend() {
        Author[] tmp = new Author[authors.length + 10];
        System.arraycopy(authors, 0, tmp, 0, authors.length);
        authors = tmp;
    }

    public void searchByName(String keyword) {
        for (int i = 0; i < size; i++) {
            if (authors[i].getName().contains(keyword) ||
                    authors[i].getSurname().contains(keyword)) {
                System.out.println(authors[i]);
            } else {
                System.out.println(AuthorBookTest.TEXT_RED + "There is not such author");
            }
        }
    }

    public void searchByAge(int minAge, int maxAge) {
        for (int i = 0; i < size; i++) {
            if (authors[i].getAge() > minAge &&
                    authors[i].getAge() < maxAge) {
                System.out.println(authors[i]);
            }
        }
    }

    public Author getByEmail(String email) {
        for (int i = 0; i < size; i++) {
            if (authors[i].getEmail().equals(email)) {
                return authors[i];
            }
        }
        return null;
    }

    public void deleteAuthor(Author author) {
        for (int i = 0; i < size; i++) {
            if (authors[i].equals(author)) {
                deleteByIndex(i);
                break;
            }
        }
    }

    public void deleteByIndex(int index) {
        for (int i = index + 1; i < size; i++) {
            authors[i - 1] = authors[i];
        }
        size--;
    }
}
