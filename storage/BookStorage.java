package authorBookProjectq.storage;

import authorBookProjectq.model.Author;
import authorBookProjectq.model.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookStorage {

    private List<Book> books = new ArrayList<>();

    public void add(Book book) {
        books.add(book);
    }

    public void print() {
        for (Book book : books) {
            System.out.println(book);
        }
    }

    public void searchByTitle(String keyword) {
        for (Book book : books) {
            if (book.getTitle().equals(keyword)) {
                System.out.println(books);
            } else {
                System.out.println("There is not such book");
            }
        }
    }

    public void searchByAuthor(Author author) {
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                System.out.println(books);
            }
        }
    }

    public Book getBySerialId(String serialId) {
        for (Book book : books) {
            if (book.getSerialId().equals(serialId)) {
                return book;
            }
        }
        return null;
    }

    public void countByAuthor(Author author) {
        int count = 0;
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                count++;
            }
        }
        System.out.println("count of " + author.getEmail() + "author's book " + count);
    }

    public void delete(Book book) {
        books.remove(book);
    }

    public void deleteByAuthor(Author author) {
        for (Book book : books) {
            if (book.getAuthor().equals(author)) {
                books.remove(book);
            }
        }
    }
}
